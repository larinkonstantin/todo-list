let app = new Vue({
    el: '#app',
    data: {
        modalOpened: false,
        taskName: '',
        tasks: [],
        search: null
    },
    created() {
        this.load();
    },
    computed: {
        filteredTasks() {
            if(!this.search)
                return this.tasks;

            return this.tasks.filter(task => task.name.toLowerCase().includes(this.search.toLowerCase()));
        }
    },
    methods: {
        toggleModal() {
            this.modalOpened = !this.modalOpened;
        },
        addTask() {
            if(this.taskName.trim() === '')
                return;

            this.tasks.push({
                name: this.taskName
            });

            this.clearForm();
            this.toggleModal();
            this.save();
        },
        clearForm() {
            this.taskName = '';
        },
        save() {
            localStorage.setItem('tasks', JSON.stringify(this.tasks));
        },
        load() {
            this.tasks = JSON.parse(localStorage.getItem('tasks')) || [];
        }
    }
});